package hig.emmasiri.fess_up;

import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

/**
 * Created by sirim_000 on 09.12.2015.
 */
public class MySQLiteHelperTest extends AndroidTestCase {

    ConfessionDB test;
    ConfessionDB copy;
    MySQLiteHelper db;




    public void setUp() throws Exception{
        super.setUp();

        db = new MySQLiteHelper(getContext());
        test = new ConfessionDB("0", "Description", "FALSE");
    }

    public void tearDown() throws Exception{}

    public void testAddGetDeleteConfession() throws Exception {

        db.addConfession(test);
        copy = db.getConfession(test.getId());
        assertEquals(true, copy.getId().equals(test.getId()));


    }

}