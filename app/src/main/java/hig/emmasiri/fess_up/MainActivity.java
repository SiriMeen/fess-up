package hig.emmasiri.fess_up;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Siri Meen
 * @Date 10.12.2015
 *
 */
public class MainActivity extends DrawerActivity {

    String myJSON;
    JSONArray confessions = null;
    ArrayList<HashMap<String, String>> confessionList;

    public static final String PREFS_NAME = "MyPrefsFile";
    private static final String TAG_RESULTS="result";
    private static final String TAG_ID = "id";
    private static final String TAG_CONF = "cText";
    private static final String TAG_LIKE ="like";
    private static final String TAG_COUNT ="CountLikes";
    private static final String TAG_TIME ="timestamp";

    Boolean like_count = false;
    Boolean newest = false;



    private static final String JSON_URL = "https://www.stud.hig.no/~130712/getConfession.php";
    private static final String ADD_LIKE = null;
    public static final String MY_JSON ="MY_JSON";

    MySQLiteHelper db;

    int confession_position = 0;

    ListView list;
    private BaseAdapter adapter;

    TextView textLike;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityIndex(0);        //Checks the activity that's active

        //Sets the layout
        FrameLayout mainContent = (FrameLayout) findViewById(R.id.mainContent);
        View view = View.inflate(this, R.layout.activity_main, mainContent);

        textLike = (TextView) findViewById(R.id.like);
        list = (ListView) findViewById(R.id.listView);
        confessionList = new ArrayList<HashMap<String,String>>();

        //Shows the tutorial pop up only once
        tutorialDialogShow();

        //DB handler
        db = new MySQLiteHelper(this);


        //Gets the actionbar(Toolbar) which is set and changes it's color
        bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF9800")));
        bar.setTitle("Toolbar");


        //Checks the sdk version and sets the color of the statusbar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#F57C00"));
        }

        //Sets the title of the activity on the toolbar
        toolbar.setTitle("");
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Fess Up");

        //Contextmenu listnere. On long click in the listview
        registerForContextMenu(list);

    }

    /**
     * Add a contextmenu an menu items
     *
     * @param menu      The context menu that is built
     * @param v         The view for which the context menu is build
     * @param menuInfo  Extra info about the item which the context menu should be shown
     *
     * Code borrowed from: http://stackoverflow.com/questions/2453620/android-how-to-find-the-position-clicked-from-the-context-menu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 0, 0, "Report");
    }

    /**
     * On report starts a new activity and sends the confessions id to the next activity
     * @param item      The context menu item that was selected
     * @return          Return true to consume normal context menu processing to proceed
     *
     * Code borrowed from: http://stackoverflow.com/questions/2453620/android-how-to-find-the-position-clicked-from-the-context-menu
     */
   @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int listPosition = info.position;
        String confessionID = confessionList.get(listPosition).get(TAG_ID);


        Intent reportPage = new Intent(MainActivity.this, ReportActivity.class);
        reportPage.putExtra("ID",confessionID);
        startActivity(reportPage);

        return true;
    }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    /**
     *
     * @param item      The menu item that was selected
     *  Sorts the listview on newest or most popular confessions
     *  Code borrowed from: http://stackoverflow.com/questions/4169714/how-to-call-activity-from-a-menu-item-in-android
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button
        switch(item.getItemId()){
            case R.id.like_sort:
                newest = false;
                like_count = true;
                confessionList.clear();
                showList();
                return true;
            case R.id.date_sort:
                like_count = false;
                newest = true;
                confessionList.clear();
                showList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    //Updates the listview everytime the activity resumes
    @Override
    protected void onResume(){
        super.onResume();
        getData();
    }

    //Clears the listview so it does not duplicate
    @Override
    protected void onPause(){
        super.onPause();
        confessionList.clear();
    }


    /**
     * AlertDialog shows up only the first time  you open the app
     * If the AlertDialog is not shown than it will be shown
     *
     * Code for the AlertDialog borrowed from http://stackoverflow.com/questions/26097513/android-simple-alert-dialog
     * Code for this borrowed from http://stackoverflow.com/questions/5409595/how-do-i-show-an-alert-dialog-only-on-the-first-run-of-my-application
     *
     */

    public void tutorialDialogShow(){

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean dialogShown = settings.getBoolean("dialogShown", false);

        if (!dialogShown) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle(MainActivity.this.getString(R.string.howToUseTitle));
            alertDialog.setMessage(MainActivity.this.getString(R.string.howToUse));
            alertDialog.setIcon(R.mipmap.ic_launcher);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            //If the AlertDialog has been shown, then don't show it
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("dialogShown", true);
            editor.apply();
        }

    }


    /**
     *
     * Gets the json object and place them in an arraylist
     * Uses a simpleAdapter to put them in the listview
     * Code borrowed from: https://www.simplifiedcoding.net/android-json-parsing-retrieve-from-mysql-database/
     */
    protected void showList(){
        try{
            JSONArray jsonArr = new JSONArray(myJSON);

            for(int i=0;i<jsonArr.length();i++){
                JSONObject c = jsonArr.getJSONObject(i);
                String id = c.getString(TAG_ID);
                String cText = c.getString(TAG_CONF);
                String count = c.getString(TAG_COUNT);
                String cTime = c.getString(TAG_TIME);


                HashMap<String, String> confession = new HashMap<String,String>();

                confession.put(TAG_ID, id);
                confession.put(TAG_CONF, cText);
                confession.put(TAG_COUNT, count);
                confession.put(TAG_TIME, cTime);

                //Checks the SQLite databse to see if the confession is stored.
                //If true, then set the textView to UNLIKE

                boolean liked = checkDatabase(id);
                String likeStatus = (liked) ? "UNLIKE" : "LIKE";
                confession.put(TAG_LIKE, likeStatus);


                confessionList.add(confession);




            }

            //Checks the amount of likes on the confessions and sorts them by most
            //Checks the date on the confessions and sort them by newest confessions
            if(like_count){
                Collections.sort(confessionList, new CountComparator());

            }else{
                Collections.sort(confessionList, new DateComparator());
            }



            //Puts the arraylist into listview
            adapter = new SimpleAdapter(
                    MainActivity.this, confessionList, R.layout.list_item,
                    new String[]{TAG_COUNT, TAG_CONF, TAG_LIKE},
                    new int[]{R.id.count, R.id.cText, R.id.like}
            );

            list.setAdapter(adapter);


            //Set a onitemclickistnere on the item in the listview
            //Code borrowed from: http://www.survivingwithandroid.com/2012/09/listviewpart-1.html#sthash.VI8YAKJD.dpuf
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                public void onItemClick(AdapterView<?> parentAdapter, View view, int position,
                                        long id) {
                    confession_position = position;
                    final String LIKE = "LIKE";
                    final String UNLIKE = "UNLIKE";

                    //Gets the position to each list item
                    HashMap<String, String> confessionItem = confessionList.get(position);

                    //Cheks to se if the textview is set to either like or unlike
                    if (confessionItem.get(TAG_LIKE).matches(LIKE)) {
                        confessionItem.put(TAG_LIKE, UNLIKE);

                        //Add a like to the count live update
                        int likeCount = Integer.parseInt(confessionItem.get(TAG_COUNT))+1;
                        confessionItem.put(TAG_COUNT, String.valueOf(likeCount));

                        //Gets the confessions id based on the position on the item in the listview
                        //And add a like to the database
                        String idC = confessionItem.get(TAG_ID);
                        addLike(idC);

                        //Add the confession to the SQLite databse
                        String conf = confessionItem.get(TAG_CONF);
                        db.addConfession(new ConfessionDB(idC, conf, "FALSE"));
                    } else {
                        //Set the textview to LIKE
                        confessionItem.put(TAG_LIKE, LIKE);

                        //Removes 1 from the like count
                        int likeCount = Integer.parseInt(confessionItem.get(TAG_COUNT))-1;
                        confessionItem.put(TAG_COUNT, String.valueOf(likeCount));

                        //Removes 1 from like count in database
                        //And removes the confession from the SQLite database
                        String idC = confessionItem.get(TAG_ID);
                        db.deleteConfession(idC);
                        deleteLike(idC);

                        List<ConfessionDB> list = db.getAllConfessions();
                    }

                    adapter.notifyDataSetChanged();

                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param id    Int id from the database
     * @return      Returns true or false if the confession is in the SQLite database or not
     */
    public boolean checkDatabase(String id){

        if(db.getConfession(id) != null){
            return true;

        }else{
            return false;
        }
    }


    /**
     * Runs a asynTask to get confessions from database and store them in result
     *
     */
    public void getData(){
        class GetDataJson extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params){

                return ConnectionManager.getDataFromURL(JSON_URL);

            }

            /**
             * Stores the result in result and runs showlist
             * @param result The result from HTTP connection
             */
            @Override
            protected void onPostExecute(String result){
                myJSON = result;
                showList();
            }

        }
        GetDataJson g = new GetDataJson();
        g.execute();
    }


    /**
     * Add a like to the database
     * @param URLid String containing the confession ID
     */
    public void addLike(String URLid){

        final String ADD_LIKE = "https://www.stud.hig.no/~130712/addLikes.php?Count="+URLid;

        class addLike extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params){


                return ConnectionManager.getDataFromURL(ADD_LIKE);

            }

            @Override
            protected void onPostExecute(String result){
                Toast.makeText(MainActivity.this,"Like added", Toast.LENGTH_SHORT).show();
            }

        }
        addLike g = new addLike();
        g.execute();
    }

    /**
     * Deletes a like in the database
     * @param URLid String containing the confession ID
     */

    public void deleteLike(String URLid){

        final String DEL_LIKE = "https://www.stud.hig.no/~130712/deleteLikes.php?Count="+URLid;

        class deleteLike extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params){


                return ConnectionManager.getDataFromURL(DEL_LIKE);

            }

            @Override
            protected void onPostExecute(String result){
                Toast.makeText(MainActivity.this,"Like removed", Toast.LENGTH_SHORT).show();
            }

        }
        deleteLike g = new deleteLike();
        g.execute();
    }


    /**
     * Class for the like count sorting
     * Code borrowed from: http://stackoverflow.com/questions/2784514/sort-arraylist-of-custom-objects-by-property
     */
    public class CountComparator implements Comparator<HashMap<String,String>> {

        /**
         *
         * @param lhs   The hashmap containing the list you want to compare
         * @param rhs   The hashmap containing the list you want to compare
         * @return      returns descending result
         */
        public int compare(HashMap<String,String> lhs, HashMap<String,String> rhs) {
            // TODO Auto-generated method stub

            Integer count1 = Integer.valueOf(lhs.get(TAG_COUNT));
            Integer count2 = Integer.valueOf(rhs.get(TAG_COUNT));

            return count2.compareTo(count1);


        }

    }

    /**
     * Class for the date sorting
     * Code borrowed from: http://stackoverflow.com/questions/13120316/how-to-sort-date-in-descending-order-from-arraylist-date-in-android
     *
     */
    public class DateComparator implements Comparator<HashMap<String,String>> {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


        /**
         *
         * @param d1    The hashmap containing the list you want to compare
         * @param d2    The hashmap containing the list you want to compare
         * @return      Returns newest to oldest
         */
        public int compare(HashMap<String,String> d1, HashMap<String,String> d2) {

            Date ndf1 = null;
            Date ndf2 = null;


            try {
                ndf1 = sdf.parse(d1.get(TAG_TIME));
                ndf2 = sdf.parse(d2.get(TAG_TIME));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return (ndf1.getTime() > ndf2.getTime() ? -1 : 1);


        }

    }

}


