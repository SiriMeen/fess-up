package hig.emmasiri.fess_up;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Emma on 12.12.2015.
 */
public class ReportActivity extends DrawerActivity {

    RadioGroup reportGroup;
    Button reportButton;
    RequestQueue requestQueue;
    String insertUrl = "https://www.stud.hig.no/~130712/addReport.php";

    String rg_select=null;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityIndex(1);

        //Sets the content
        FrameLayout mainContent = (FrameLayout) findViewById(R.id.mainContent);
        View view = View.inflate(this, R.layout.activity_report, mainContent);


        reportGroup = (RadioGroup) findViewById(R.id.reportGroup);
        reportButton = (Button) findViewById(R.id.reportButton);


        /**
         * Checks if a radio-button is selected
         * rg_select is the id that is sent to the database
         **/
        reportGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){

                    case (R.id.report1):
                        rg_select = "1";
                        break;

                    case (R.id.report2):
                        rg_select = "2";
                        break;

                    case (R.id.report3):
                        rg_select = "3";
                        break;

                    case (R.id.report4):
                        rg_select = "4";
                        break;
                }
            }
        });


        //adds the toolbar and the colour for the activity
        bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2196F3")));
        bar.setTitle("Toolbar");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#1976D2"));
        }

        toolbar.setTitle("");
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Report Confession");

        final String text = getIntent().getStringExtra("ID");
        final Integer CID = Integer.parseInt(text.toString());



        requestQueue = Volley.newRequestQueue(getApplicationContext());

        /**
         * When clicking button "reportButton" it takes the chosen report-message and confessionId
         * and adds to the database using PHP-file addReport.
         *
         * Code borrowed from https://www.youtube.com/watch?v=H-SE1m_A-SA
         *
         **/

        reportButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {

                if (rg_select==null) {

                //If no option is selected, tell user to select
                    Toast.makeText(ReportActivity.this, "Pick one of the options", Toast.LENGTH_SHORT).show();
                    return;

                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            //if option is selected, let the user know and return to MainActivity
                            reportGroup.getCheckedRadioButtonId();
                            Toast.makeText(getApplicationContext(), "Thank you! The confession has been reported.", Toast.LENGTH_LONG).show();

                            Intent homepage = new Intent(ReportActivity.this, MainActivity.class);
                            startActivity(homepage);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        //Takes the parameters messageID and confessionID and sends to the database
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> parameters = new HashMap<String, String>();
                            parameters.put("mID", rg_select);
                            parameters.put("cID", CID.toString());

                            return parameters;
                        }
                    };
                    requestQueue.add(request);
                }
            }

        });


    }




}


