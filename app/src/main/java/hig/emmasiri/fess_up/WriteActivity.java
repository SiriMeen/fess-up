package hig.emmasiri.fess_up;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Created by Emma on 08.12.2015.
 */

public class WriteActivity extends DrawerActivity {

    EditText confession;
    Button insert;
    RequestQueue requestQueue;
    String insertUrl = "https://www.stud.hig.no/~130712/addConfession.php";

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityIndex(1);

        //Sets the content
        FrameLayout mainContent = (FrameLayout) findViewById(R.id.mainContent);
        View view = View.inflate(this, R.layout.activity_write, mainContent);


        confession = (EditText) findViewById(R.id.editText);
        insert = (Button) findViewById(R.id.insert);

        //adds the toolbar and the colour for the activity
        bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#E91E63")));
        bar.setTitle("Toolbar");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#C2185B"));
        }

        toolbar.setTitle("");
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("New Confession");



        requestQueue = Volley.newRequestQueue(getApplicationContext());

        /**
         * When clicking button "insert" it takes parameter confession and adds to the database using PHP-file addConfession.
         *
         * Code borrowed from https://www.youtube.com/watch?v=H-SE1m_A-SA
         *
         **/
        insert.setOnClickListener(new View.OnClickListener() {
            String sConfession;

            @Override
            public void onClick(View view) {
                sConfession = confession.getText().toString();
                if (sConfession.matches("")) {

                    //if nothing is written, don't do anything, just let the user know with a Toast
                    Toast.makeText(WriteActivity.this, "You did not enter a confession", Toast.LENGTH_SHORT).show();
                    return;

                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            //if confession is written, show toast and take user back to MainActivity
                            confession.setText("");
                            Toast.makeText(getApplicationContext(), "Confession has been added!", Toast.LENGTH_LONG).show();
                            Intent homepage = new Intent(WriteActivity.this, MainActivity.class);
                            startActivity(homepage);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        //Takes parameter confession and puts text into cText-string to the database
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> parameters = new HashMap<String, String>();
                            parameters.put("cText", confession.getText().toString());

                            return parameters;
                        }
                    };
                    requestQueue.add(request);
                }
            }

        });


    }


}


