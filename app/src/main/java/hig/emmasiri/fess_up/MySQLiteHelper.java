package hig.emmasiri.fess_up;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.Key;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Siri Meen
 * @Date 09.12.2015
 *
 * Code borrowed from: Code borrowed from: http://hmkcode.com/android-simple-sqlite-database-tutorial/
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ConfessionsDB";

    // Confession table name
    private static final String TABLE_CONFESSION = "confession";

    // Confession Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_CTEXT = "cText";
    private static final String KEY_LIKE = "like";

    private static final String[] COLUMNS = {KEY_ID,KEY_CTEXT,KEY_LIKE};

    public MySQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     *
     * @param db    Database connection
     *
     *  Creates the database
     */
    @Override
    public void onCreate(SQLiteDatabase db){

        //SQL query to create the table
        String CREATE_CONFESSION_TABLE = "CREATE TABLE confession("+
                "id TEXT PRIMARY KEY, "+
                "cText TEXT, "+
                "like TEXT)";

        db.execSQL(CREATE_CONFESSION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        //SQL query to drop the table
        db.execSQL("DROP TABLE IF EXISTS confession");

        this.onCreate(db);
    }


     //Add a confession to the database
    public void addConfession(ConfessionDB confession){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, confession.getId());
        values.put(KEY_CTEXT, confession.getcText());
        values.put(KEY_LIKE, confession.getLike());

        db.insert(TABLE_CONFESSION, null, values);

        db.close();
    }



    /**
     * Gets a spesific confession based on the ID
     * @param id    String ID from the confession
     * @return      Returns the confession
     */
    public ConfessionDB getConfession(String id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONFESSION, COLUMNS,
                "id =?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null);

        if(cursor.getCount() > 0){
            cursor.moveToFirst();

            ConfessionDB confession = new ConfessionDB();
            confession.setId(cursor.getString(0));
            confession.setcText(cursor.getString(1));
            confession.setLike(cursor.getString(2));

            cursor.close();
            return confession;
        }else{
            cursor.close();
            return null;
        }



    }

    //Gets all confessions in the DB
    public List<ConfessionDB> getAllConfessions(){
        List<ConfessionDB> confessions = new LinkedList<ConfessionDB>();

        String query = "SELECT * FROM "+ TABLE_CONFESSION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ConfessionDB confession = null;
        if(cursor.moveToFirst()){
            do{
                confession = new ConfessionDB();
                confession.setId(cursor.getString(0));
                confession.setcText(cursor.getString(1));
                confession.setLike(cursor.getString(2));

                confessions.add(confession);
            }while(cursor.moveToNext());
        }

        cursor.close();
        return confessions;

    }

    //Deletes a confession based on the ID
    public void deleteConfession(String id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_CONFESSION,
                KEY_ID+" = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

}
