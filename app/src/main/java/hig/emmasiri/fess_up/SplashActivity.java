package hig.emmasiri.fess_up;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Handler;

/**
 * Created by Emma on 11.12.2015.
 */
public class SplashActivity extends AppCompatActivity {

    /**
     * Code for adding the splash screen is borrowed from: http://stackoverflow.com/questions/5486789/how-do-i-make-a-splash-screen
     **/

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);

        /**
         *  New Handler to start the MainActivity
         * and close this Splash-Screen after some seconds.
         **/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /**
                 *Create an Intent that will start the MainActivity
                 **/
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}