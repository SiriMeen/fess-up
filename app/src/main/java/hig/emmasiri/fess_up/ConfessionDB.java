package hig.emmasiri.fess_up;

/**
 *
 * @author Siri Meen
 * @Date 09.12.2015
 *
 * SQLiteDatabase for storing local confessions and prevent a user from liking the post more than one time
 * Code borrowed from: http://hmkcode.com/android-simple-sqlite-database-tutorial/
 */
public class ConfessionDB {

    /**
     * id = The confession id
     * cText = The confession text
     * like = Sets the like to false in the database
     */
    private  String id;
    private String cText;
    private String like;

    public ConfessionDB(){}

    public ConfessionDB(String id, String cText, String like){
        super();
        this.id = id;
        this.cText = cText;
        this.like = like;
    }

    //Getters

    @Override
    public String toString(){
        return "Confession [id= "+ id + ", text= "+ cText + ", like= "+like+"]";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getcText() {
        return cText;
    }

    public void setcText(String cText) {
        this.cText = cText;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }
}
