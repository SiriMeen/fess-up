package hig.emmasiri.fess_up;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Siri Meen
 * @Date 09.12.2015
 *
 * Class for statactivity
 * Code borrowed from: http://www.android-graphview.org/documentation/category/how-to-create-a-simple-graph
 */
public class StatActivity extends DrawerActivity {

    String gData;
    JSONArray gDataArray;
    ArrayList<HashMap<String, String>> gDataList;
    String GraphViewData[];
    DataPoint[] dataPoints;
    GraphView graph;
    String[] labels;

    Boolean Year = false;
    Boolean Month = true;
    Boolean Week = false;

    String STAT_URL_YEAR = "http://www.stud.hig.no/~130703/getStatistics.php?Year=true";
    String STAT_URL_MONTH = "http://www.stud.hig.no/~130703/getStatistics.php?Month=true";
    String STAT_URL_WEEK = "http://www.stud.hig.no/~130703/getStatistics.php";

    private static final String TAG_MONTH = "_Month";
    private static final String TAG_CMONTH = "_CMonth";
    private static final String TAG_DAY = "_Day";
    private static final String TAG_COUNT = "_Count";
    private static final String TAG_YEAR = "_Year";

    LineGraphSeries<DataPoint> statistics;
    StaticLabelsFormatter staticLabelsFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityIndex(2); //Checks the activity that's active

        FrameLayout mainContent = (FrameLayout) findViewById(R.id.mainContent);
        View view = View.inflate(this, R.layout.activity_stat, mainContent);

        bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#4CAF50")));
        bar.setTitle("Toolbar");

        gDataList = new ArrayList<HashMap<String,String>>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#388E3C"));
        }

        toolbar.setTitle("");
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Statistics");

        getStatData(STAT_URL_MONTH);       //Monthly data is the standard showing

        graph = (GraphView) findViewById(R.id.graph);

        staticLabelsFormatter = new StaticLabelsFormatter(graph);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the setting button
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter());
        gDataList.clear();
        graph.removeAllSeries();
        statistics.resetData(dataPoints);
        switch(item.getItemId()){
            case R.id.Stat_Week:
                Year = false;
                Month = false;
                Week = true;
                getStatData(STAT_URL_WEEK);
                return true;
            case R.id.Stat_Month:
                Year = false;
                Week = false;
                Month = true;
                getStatData(STAT_URL_MONTH);
                return true;
            case R.id.Stat_Year:
                Year = true;
                Month = false;
                Week = false;
                getStatData(STAT_URL_YEAR);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Show the data and puts the data into the GraphView
     */
    public void showData() {

        //Checks if month of week is true
        if(Month || Week){

            //Iterates through the json data and stores them in a arraylist
            try{
                JSONArray jsonArr = new JSONArray(gData);

                for(int i=0;i<jsonArr.length();i++){
                    JSONObject c = jsonArr.getJSONObject(i);
                    String _Month = c.getString(TAG_MONTH);
                    String _Day = c.getString(TAG_DAY);
                    String _Count = c.getString(TAG_COUNT);

                    HashMap<String, String> statistic = new HashMap<String,String>();

                    statistic.put(TAG_MONTH, _Month);
                    statistic.put(TAG_DAY, _Day);
                    statistic.put(TAG_COUNT, _Count);

                    gDataList.add(statistic);

                }

                if(gDataList != null){

                    dataPoints = new DataPoint[gDataList.size()];
                    for (int i = 0; i < gDataList.size(); i++) {

                        Integer day = Integer.parseInt(gDataList.get(i).get(TAG_DAY));
                        Integer count = Integer.parseInt(gDataList.get(i).get(TAG_COUNT));

                        dataPoints[i] = new DataPoint(day, count);
                    }
                    graph.getGridLabelRenderer().resetStyles();

                    statistics = new LineGraphSeries<DataPoint>(dataPoints);

                    //Add the data to the graph
                    graph.addSeries(statistics);

                }
            }catch(Exception e){
                e.printStackTrace();
            }

        }else {

            //Year data
            try{
                JSONArray jsonArr = new JSONArray(gData);

                for(int i=0;i<jsonArr.length();i++){
                    JSONObject c = jsonArr.getJSONObject(i);
                    String _Month = c.getString(TAG_MONTH);
                    String _CMonth = c.getString(TAG_CMONTH);
                    String _Year = c.getString(TAG_YEAR);
                    String _Count = c.getString(TAG_COUNT);


                    HashMap<String, String> statistic = new HashMap<String,String>();

                    statistic.put(TAG_MONTH, _Month);
                    statistic.put(TAG_CMONTH, _CMonth);
                    statistic.put(TAG_YEAR, _Year);
                    statistic.put(TAG_COUNT, _Count);

                    gDataList.add(statistic);

                }

                if(gDataList != null){
                    dataPoints = new DataPoint[gDataList.size()];
                    labels = new String[gDataList.size()];

                    for (int i = 0; i < gDataList.size(); i++) {

                        Integer month = Integer.parseInt(gDataList.get(i).get(TAG_MONTH));
                        Integer count = Integer.parseInt(gDataList.get(i).get(TAG_COUNT));

                        String cMonth = gDataList.get(i).get(TAG_CMONTH);

                        dataPoints[i] = new DataPoint(month, count);
                        labels[i] = new String(cMonth);
                    }

                    statistics = new LineGraphSeries<DataPoint>(dataPoints);

                    graph.addSeries(statistics);

                    //Sets a static label on the horizontal axsis
                    staticLabelsFormatter.setHorizontalLabels(labels);
                    graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    //Sets up a connection and runs a asynctask to get jsondata from the database server
    public void getStatData(final String url){
        class GetStatJson extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params){

                return ConnectionManager.getDataFromURL(url);
            }

            @Override
            protected void onPostExecute(String result){
                gData = result;
                showData();

            }
        }
        GetStatJson g = new GetStatJson();
        g.execute();
    }

}
