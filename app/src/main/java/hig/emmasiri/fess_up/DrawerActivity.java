package hig.emmasiri.fess_up;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * @author Siri Meen
 * @Date 07.12.2015
 *
 * This class handles the navigation drawer
 * Code borrowed from:http://codetheory.in/android-navigation-drawer/
 */

public class DrawerActivity extends AppCompatActivity {

    private static String TAG = DrawerActivity.class.getSimpleName();

    ListView mDrawerList;                       //ListView which contains the menu
    RelativeLayout mDrawerPane;                 //Drawer Navigation
    public ActionBarDrawerToggle mDrawerToggle; //Toggle button on toolbar
    public DrawerLayout mDrawerLayout;          //The layout for the drawer

    private int index;                          //Usedto check the index of the activities
    public ActionBar bar;                       //Used for the toggle button

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();//Arraylist containing the navigation items

    public Toolbar toolbar;                     //The custom tollbar

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);                   //Sets the toolbar as a actionbar



         //Adds the items to the navigationdrawer

        mNavItems.add(new NavItem("Home", R.drawable.sbtn4));
        mNavItems.add(new NavItem("Write", R.drawable.sbtn1));
        mNavItems.add(new NavItem("Statistics", R.drawable.sbtn3));
        mNavItems.add(new NavItem("Help", R.drawable.sbtn2));

        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);


        /**
         * Drawer item onclicklistnere
         * When clicked selectItemfromDrawer runs
         */
       mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           /**
            *
            * @param parent     The AdapterView were the click happend
            * @param view       The view within the Adapterview thats was clicked
            * @param position   The position of the view int the adapter
            * @param id         The row id of the item that was clicked
            */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        /**
         * Drawer listener which checks to see if the drawer is open or closed
         */

        mDrawerToggle = new ActionBarDrawerToggle(DrawerActivity.this, mDrawerLayout, R.string.open, R.string.close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                invalidateOptionsMenu();
            }
        };

        //Sets the drawer listnere to the drawer
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    // Called when invalidateOptionsMenu() is invoked
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerPane);
       // menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    /**
     *
     * @param item  The menu item that was selected
     * @return      Returns true to consume the normal menu processing to proceed
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *
     *
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    /**
     *
     * @param index     Takes a int to see which activity is selected
     */
    protected void setActivityIndex(int index){
        this.index = index;
    }

    /**
     *
     * @param position  Takes the position of the item in the listview in the drawer
     *
     * This selects the item from the drawer and starts a activity
     * Also checks to see if the activity is already selected
     */
    private void selectItemFromDrawer(int position) {
        Intent intent = null;

        switch (position) {
            case 0:
                intent = new Intent(this, MainActivity.class);
                break;
            case 1:
                intent = new Intent(this, WriteActivity.class);
                break;
            case 2:
                intent = new Intent(this, StatActivity.class);
                break;
            case 3:
                intent = new Intent(this, HelpActivity.class);
                break;
            default:
                break;
        }
        if(position != index && position != 0) {
            this.startActivity(intent);
        }
        if(index != 0 && position != index){
            finish();
        }

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerPane);
    }


    /**
     * Class for the navigation item
     */

    class NavItem {
        String mTitle;
        int mIcon;

        public NavItem(String title, int icon) {
            mTitle = title;
            mIcon = icon;

        }
    }


    /**
     * Custom adapter for the navigation drawer
     */

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        /**
         *
         * @param context   The activity context
         * @param navItems  The arraylist containing the navigation items
         */
        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        /**
         * Getters
         */

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        /**
         *
         * @param position      Index of the item whose view we clicked
         * @param convertView   The old view
         * @param parent        The parent that the view will be attached to
         * @return              The view
         */

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.drawer_item, null);
            }
            else {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);

            titleView.setText( mNavItems.get(position).mTitle );
            iconView.setImageResource(mNavItems.get(position).mIcon);

            return view;
        }
    }
}
