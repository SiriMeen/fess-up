package hig.emmasiri.fess_up;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Siri Meen
 * @Date 08.12.2015
 *

 * Manager for HTTP connection
 * Code borrowed from: https://www.simplifiedcoding.net/android-json-tutorial-to-get-data-from-mysql-database/
 */
public final class ConnectionManager {

    private ConnectionManager(){}


    /**
     *
     * @param sURL URL with a php script
     * @return Returns the result of the PHP script
     */
    public static String getDataFromURL(final String sURL){

        String result = null;

        BufferedReader bufferedReader = null;
        try{
            URL url = new URL(sURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();

            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String json;
            while((json = bufferedReader.readLine())!=null){
                sb.append(json+"\n");
            }

            result = sb.toString();

            return result;


        }catch(Exception e){
            return null;
        }

    }

}
